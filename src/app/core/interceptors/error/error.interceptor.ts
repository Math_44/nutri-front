import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, flatMap, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpService } from 'src/app/services/http.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private authenticationService: AuthenticationService,
    private httpService: HttpService,
    private router: Router
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((responseOne: any) => {
        if (responseOne.status === 401) {
          const value = {
            refreshToken:
              this.authenticationService.getAuthentication().refresh_token,
          };
          return this.httpService.post('Login', value).pipe(
            tap(() => this.authenticationService.unsetAuthentication()),
            flatMap((responseTwo: any) => {
              this.authenticationService.setAuthentication(
                responseTwo.data,
                false
              );
              req = req.clone({
                setHeaders: {
                  Authorization: `Bearer ${responseTwo.token}`,
                },
              });
              return next.handle(req);
            }),
            catchError((responseTwo: any) => {
              this.router.navigate(['/']);
              return throwError(responseTwo.error);
            })
          );
        }
        return throwError(responseOne.error || responseOne);
      })
    );
  }
}
