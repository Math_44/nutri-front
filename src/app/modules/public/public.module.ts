import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { HomeComponent } from './pages/home/home.component';
import { BiografiaComponent } from './pages/biografia/biografia.component';
import { ServicosComponent } from './pages/servicos/servicos.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContatoComponent } from './pages/contato/contato.component';
import { HttpClientModule } from '@angular/common/http';
import { AgendarConsultaComponent } from './pages/agendar-consulta/agendar-consulta.component';
import { NgxMaskModule } from 'ngx-mask';
import { ComoFuncionaComponent } from './pages/como-funciona/como-funciona.component';

@NgModule({
  declarations: [
    PublicComponent,
    HomeComponent,
    BiografiaComponent,
    ServicosComponent,
    ContatoComponent,
    AgendarConsultaComponent,
    ComoFuncionaComponent,
  ],
  imports: [
    CommonModule,
    NgxMaskModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    PublicRoutingModule,
    SharedModule,
  ],
})
export class PublicModule {}
