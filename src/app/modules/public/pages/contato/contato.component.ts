import { Mensagens } from './../../../../core/classes/Mensagens';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html',
  styleUrls: ['./contato.component.scss'],
})
export class ContatoComponent implements OnInit {
  form!: FormGroup;
  loading: boolean = false;

  constructor(
    private httpService: HttpService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      nome: [null, [Validators.required]],
      email: [null, [Validators.email, Validators.required]],
      telefone: [null, [Validators.required]],
      mensagem: [null, [Validators.required]],
    });
  }

  enviarContato() {
    if (this.form.valid) {
      this.loading = true;

      this.httpService
        .post('contatos', this.form.value)
        .subscribe((data: any) => {
          this.loading = false;
          Mensagens.sucesso(
            'Enviado com sucesso, entraremos em contato em breve.'
          );
        });
    } else {
      this.loading = false;
      Mensagens.erro('Preecha o formulário corretamente');
    }
  }
}
