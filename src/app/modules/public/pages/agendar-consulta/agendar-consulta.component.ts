import { Mensagens } from './../../../../core/classes/Mensagens';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { date } from './../../../../core/classes/date.function';
import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-agendar-consulta',
  templateUrl: './agendar-consulta.component.html',
  styleUrls: ['./agendar-consulta.component.scss'],
})
export class AgendarConsultaComponent implements OnInit {
  pergunta: any;
  datas: any;
  loading: boolean = false;
  respostas: any = [];
  formulario!: FormGroup;

  constructor(private http: HttpService, private formbuilder: FormBuilder) {}

  ngOnInit(): void {
    this.getPerguntas();

    this.formulario = this.formbuilder.group({
      nome: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      telefone: [null, [Validators.required]],
      data_nascimento: [null, [Validators.required]],
      form: [null, [Validators.required]],
    });
  }

  getPerguntas() {
    this.http.get('perguntas').subscribe((data: any) => {
      this.pergunta = data;
      console.log(data);
    });
  }

  convertDate(data: any) {
    return date(data);
  }

  gravarRespostas(event: any, id: any) {
    this.respostas.push({ id: id, resposta: event.target.value });
  }

  enviar() {
    const body = {
      nome: this.formulario.value.nome,
      telefone: this.formulario.value.telefone,
      email: this.formulario.value.email,
      data_nascimento: this.formulario.value.data_nascimento,
      form: this.respostas.map((r: any) => ({
        descResposta: r.resposta,
        pergunta_id: r.id,
      })),
    };

    if (this.pergunta.length <= this.respostas.length) {
      this.loading = true;

      this.http.post('questionario-preconsulta', body).subscribe(
        (data) => {
          Mensagens.sucesso(
            'Obrigado pelas respostas, entrarei em contato em breve!'
          );
          this.loading = false;
        },
        (err) => {
          Mensagens.erro('Erro ao enviar o formulário');
          this.loading = false;
        }
      );
    } else {
      Mensagens.erro('Preencha o formulario corretamente');
    }
  }
}
