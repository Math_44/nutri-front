import { ComoFuncionaComponent } from './pages/como-funciona/como-funciona.component';
import { AgendarConsultaComponent } from './pages/agendar-consulta/agendar-consulta.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BiografiaComponent } from './pages/biografia/biografia.component';
import { ContatoComponent } from './pages/contato/contato.component';
import { HomeComponent } from './pages/home/home.component';
import { ServicosComponent } from './pages/servicos/servicos.component';

import { PublicComponent } from './public.component';

const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
      },

      {
        path: 'biografia',
        component: BiografiaComponent,
      },

      {
        path: 'servicos',
        component: ServicosComponent,
      },

      {
        path: 'contato',
        component: ContatoComponent,
      },

      {
        path: 'questionario',
        component: AgendarConsultaComponent,
      },

      {
        path: 'como-funciona',
        component: ComoFuncionaComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicRoutingModule {}
