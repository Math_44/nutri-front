import { PainelAdminComponent } from './pages/painel-admin/painel-admin.component';
import { LoginComponent } from './pages/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrivateComponent } from './private.component';
import { VisualizarRespostasComponent } from './pages/visualizar-respostas/visualizar-respostas.component';
import { AuthGuardsService } from 'src/app/services/guards/auth-guards.service';

const routes: Routes = [
  {
    path: '',
    component: PrivateComponent,
    children: [
      { path: 'login', component: LoginComponent },

      {
        path: 'painel-admin',
        component: PainelAdminComponent,
        canActivate: [AuthGuardsService],
      },

      {
        path: 'respostas/:id',
        component: VisualizarRespostasComponent,
        canActivate: [AuthGuardsService],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivateRoutingModule {}
