import { Mensagens } from './../../../../core/classes/Mensagens';
import { Router } from '@angular/router';
import { AuthenticationService } from './../../../../services/authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.scss'],
})
export class FormLoginComponent implements OnInit {
  form!: FormGroup;
  loading: boolean = false;

  constructor(
    private formbuilder: FormBuilder,
    private http: HttpService,
    private autenticationService: AuthenticationService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.form = this.formbuilder.group({
      email: [null, [Validators.email, Validators.required]],
      senha: [null, [Validators.required, Validators.minLength(6)]],
    });
  }

  login() {
    this.loading = true;

    if (this.form.valid) {
      this.http.post('Login', this.form.value).subscribe(
        (data) => {
          this.loading = false;
          this.autenticationService.setAuthentication(
            data.token,
            data.usuario[0].nome
          );
          this.route.navigate(['app/painel-admin']);
          Mensagens.sucesso('Login feito com sucesso');
        },

        (err) => {
          this.loading = false;
          Mensagens.erro(err.error);
        }
      );
    } else {
      this.loading = false;
      Mensagens.erro('Preencha o formulario Corretamente');
    }
  }
}
