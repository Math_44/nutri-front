import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Mensagens } from 'src/app/core/classes/Mensagens';

@Component({
  selector: 'app-private',
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.scss'],
})
export class PrivateComponent implements OnInit {
  constructor(private auth: AuthenticationService) {}

  token = this.auth.token;

  ngOnInit(): void {

    if (this.tokenExpired(this.token)) {
      this.auth.logout();
      Mensagens.erro('Sessão encerrada.');
    }

  }

  tokenExpired(token: string) {
    const expiry = JSON.parse(atob(token.split('.')[1])).exp;
    return Math.floor(new Date().getTime() / 1000) >= expiry;
  }

}
