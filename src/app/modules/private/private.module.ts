import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivateRoutingModule } from './private-routing.module';
import { PrivateComponent } from './private.component';
import { LoginComponent } from './pages/login/login.component';
import { PainelAdminComponent } from './pages/painel-admin/painel-admin.component';
import { VisualizarRespostasComponent } from './pages/visualizar-respostas/visualizar-respostas.component';
import { FormLoginComponent } from './components/form-login/form-login.component';
import { NgxMaskModule } from 'ngx-mask';
import { VisualizarContatosComponent } from './pages/visualizar-contatos/visualizar-contatos.component';

@NgModule({
  declarations: [
    PrivateComponent,
    FormLoginComponent,
    LoginComponent,
    PainelAdminComponent,
    VisualizarRespostasComponent,
    VisualizarContatosComponent
  ],
  imports: [
    CommonModule,
    PrivateRoutingModule,
    NgxMaskModule.forRoot(),
    SharedModule,
    ReactiveFormsModule,
    NgbModule,
    FormsModule,
  ],
})
export class PrivateModule {}
