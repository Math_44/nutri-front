import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarRespostasComponent } from './visualizar-respostas.component';

describe('VisualizarRespostasComponent', () => {
  let component: VisualizarRespostasComponent;
  let fixture: ComponentFixture<VisualizarRespostasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VisualizarRespostasComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarRespostasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
