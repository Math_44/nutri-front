import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-visualizar-respostas',
  templateUrl: './visualizar-respostas.component.html',
  styleUrls: ['./visualizar-respostas.component.scss'],
})
export class VisualizarRespostasComponent implements OnInit {
  respostas: any;
  usuario: any;

  constructor(
    private http: HttpService,
    private route: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.http.get(`questionario-preconsulta/${id}`).subscribe((data: any) => {
      this.respostas = data;
    });

    this.http.get(`usuarios/${id}`).subscribe((data: any) => {
      this.usuario = data;
    });
  }
}
