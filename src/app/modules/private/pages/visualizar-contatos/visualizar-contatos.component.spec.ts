import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarContatosComponent } from './visualizar-contatos.component';

describe('VisualizarContatosComponent', () => {
  let component: VisualizarContatosComponent;
  let fixture: ComponentFixture<VisualizarContatosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VisualizarContatosComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarContatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
