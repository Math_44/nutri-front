import { HttpService } from './../../../../services/http.service';
import { Component, OnInit } from '@angular/core';
import { Mensagens } from 'src/app/core/classes/Mensagens';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-visualizar-contatos',
  templateUrl: './visualizar-contatos.component.html',
  styleUrls: ['./visualizar-contatos.component.scss'],
})
export class VisualizarContatosComponent implements OnInit {

  contatos: any = [];
  loading = false;

  constructor(private http: HttpService) { }

  ngOnInit(): void {

    this.getContatos();

  }


  getContatos() {

    this.loading = true;
    this.http.get('contatos').subscribe((data: any) => {
      this.contatos = data;
      this.loading = false;
    })


  }

  excluir(item: any): void {
    Swal.fire({
      title: 'Excluir solicitação de contato ?',
      text: `Deseja mesmo excluir a solicitação de contato de ${item.nome}?`,
      showCancelButton: true,
      reverseButtons: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: `Sim`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.http.delete(`contatos/${item.id}`).subscribe(
          (data: any) => {
            Mensagens.sucesso('Excluido com sucesso')
            this.getContatos();
          },
          (err: any) => {
            Mensagens.sucesso('Excluido com sucesso')
            this.getContatos();
          }

        )
      }
    });
  }


}
