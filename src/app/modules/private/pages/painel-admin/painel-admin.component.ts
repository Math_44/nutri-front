import { Mensagens } from './../../../../core/classes/Mensagens';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-painel-admin',
  templateUrl: './painel-admin.component.html',
  styleUrls: ['./painel-admin.component.scss'],
})
export class PainelAdminComponent implements OnInit {
  d = new Date();
  hour = this.d.getHours();
  frase!: string;
  nomeUsuario!: string;
  pacientes: any = [];
  inicial!: NgbDateStruct;
  final!: NgbDateStruct;
  loading: boolean = false;
  content: any = 0;

  constructor(
    private autenticationService: AuthenticationService,
    private http: HttpService
  ) { }

  ngOnInit(): void {
    this.nomeUsuario = this.autenticationService.getUser();
    this.mensagem();
    this.listarQuestionarios();
  }

  mensagem() {
    if (this.hour < 5) {
      this.frase = 'Boa Noite';
    } else if (this.hour < 8) {
      this.frase = 'Bom Dia';
    } else if (this.hour < 12) {
      this.frase = 'Bom Dia';
    } else if (this.hour < 18) {
      this.frase = 'Boa tarde';
    } else {
      this.frase = 'Boa noite';
    }
  }

  typeContent(type: any) {
    this.content = type;
  }

  typesContet = [
    { name: 'Respostas do questionário', type: 0 },
    { name: 'Contatos', type: 1 },
  ];

  listarQuestionarios() {
    this.loading = true;
    this.http.get('usuarios').subscribe(
      (data) => {
        this.pacientes = data;
        this.loading = false;
      },
      (err) => {
        this.loading = false;
      }
    );
  }


  excluir(item: any): void {
    Swal.fire({
      title: 'Excluir paciente ?',
      text: `Deseja mesmo excluir o paciente ${item.nome}?`,
      showCancelButton: true,
      reverseButtons: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: `Sim`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.http.delete(`usuarios/${item.id}`).subscribe(
          (data: any) => {
            Mensagens.sucesso('Excluido com sucesso')
            this.listarQuestionarios();
          },
          (err: any) => {
            Mensagens.sucesso('Excluido com sucesso')
            this.listarQuestionarios();
          }

        )
      }
    });
  }

  listarPorData() {
    const dataInicial = `${this.inicial.year}-${this.inicial.month}-${this.inicial.day}`;
    const dataFinal = `${this.final.year}-${this.final.month}-${this.final.day}`;
    this.loading = true;
    this.http.get(`usuarios/${dataInicial}/${dataFinal}`).subscribe(
      (data) => {
        this.pacientes = data;
        this.loading = false;
      },
      (err) => {
        this.loading = false;
      }
    );
  }
}
