import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {

  constructor(private route: Router) {}


  setAuthentication(token: string, usuario: any): void {
    localStorage.setItem('token', token);
    sessionStorage.setItem('token', token);
    localStorage.setItem('usuario', usuario);
    sessionStorage.setItem('usuario', usuario);
  }

  getAuthentication(): any {
    const token = localStorage.getItem('token') as string;
    const session = sessionStorage.getItem('token') as string;
    return token || session;
  }

  getUser(): any {
    const token = localStorage.getItem('usuario') as string;
    const session = sessionStorage.getItem('usuario') as string;
    return token || session;
  }

  unsetAuthentication(): void {
    localStorage.removeItem('token');
    sessionStorage.removeItem('token');
  }

  isLoggedIn(): boolean {
    const local = localStorage.getItem('token');
    const session = sessionStorage.getItem('token');
    return local || session ? true : false;
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    this.route.navigate(['app/login']);
  }

  get token(): string {
    return localStorage.getItem('token') || '';
  }

}
