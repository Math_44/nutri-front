import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Mensagens } from '../core/classes/Mensagens';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  url = environment.url;

  constructor(private http: HttpClient) {}

  get(route: string, successMessage = false, options?: any): Observable<any> {
    return options
      ? this.http.get(`${this.url}/${route}`, options)
      : this.http.get(`${this.url}/${route}`).pipe(
          take(1),
          tap(
            ({ message }: any) => {
              if (successMessage) Mensagens.sucesso(message);
            },
            ({ message }) => Mensagens.erro(message)
          )
        );
  }

  post(
    route: string,
    value: any,
    successMessage = false,
    options?: any
  ): Observable<any> {
    return options
      ? this.http.post(`${this.url}/${route}`, value, options)
      : this.http.post(`${this.url}/${route}`, value).pipe(
          take(1),
          tap(
            ({ message }: any) => {
              if (successMessage) Mensagens.sucesso(message);
            },
            ({ message }) => Mensagens.erro(message)
          )
        );
  }

  put(
    route: string,
    value: any,
    successMessage = false,
    options?: any
  ): Observable<any> {
    return options
      ? this.http.put(`${this.url}/${route}`, value, options)
      : this.http.put(`${this.url}/${route}`, value).pipe(
          take(1),
          tap(
            ({ message }: any) => {
              if (successMessage) Mensagens.sucesso(message);
            },
            ({ message }) => Mensagens.erro(message)
          )
        );
  }

  delete(
    route: string,
    successMessage = false,
    options?: any
  ): Observable<any> {
    return options
      ? this.http.delete(`${this.url}/${route}`, options)
      : this.http.delete(`${this.url}/${route}`).pipe(
          take(1),
          tap(
            ({ message }: any) => {
              if (successMessage) Mensagens.sucesso(message);
            },
            ({ message }) => Mensagens.erro(message)
          )
        );
  }
}
