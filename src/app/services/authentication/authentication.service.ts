import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  setAuthentication(token: string, rememberMe: boolean): void {
    rememberMe
      ? localStorage.setItem('token', token)
      : sessionStorage.setItem('token', token);
  }

  getAuthentication(): any {
    const token = localStorage.getItem('token') as string;
    const session = sessionStorage.getItem('token') as string;
    return token || session;
  }

  unsetAuthentication(): void {
    localStorage.removeItem('token');
    sessionStorage.removeItem('token');
  }

  isLoggedIn(): boolean {
    const local = localStorage.getItem('token');
    const session = sessionStorage.getItem('token');
    return local || session ? true : false;
  }
}
