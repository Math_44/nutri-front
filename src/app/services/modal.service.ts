import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalPerguntasComponent } from '../shared/component/modal-perguntas/modal-perguntas.component';

@Injectable({
  providedIn: 'root',
})
export class ModalService {
  constructor(private modal: NgbModal) {}

  openPerguntas() {
    this.modal.open(ModalPerguntasComponent, {
      windowClass: 'dark-modal',
      size: 'lg',
    });
  }
}
