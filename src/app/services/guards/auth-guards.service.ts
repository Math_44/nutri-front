import { AuthenticationService } from 'src/app/services/authentication.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Mensagens } from 'src/app/core/classes/Mensagens';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardsService {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  canActivate(): boolean {
    if (this.authenticationService.isLoggedIn()) {
      return true;
    } else {
      Mensagens.erro('Faça o login');
      this.router.navigate(['/app/login']);
      return false;
    }
  }
}
