import { AbasComponent } from './component/abas/abas.component';
import { RouterModule } from '@angular/router';
import { LoadingComponent } from './component/loading/loading.component';
import { ControlErrorComponent } from './component/control-error/control-error.component';
import { ModalPerguntasComponent } from './component/modal-perguntas/modal-perguntas.component';
import { FooterComponent } from './template/footer/footer.component';
import { NavBarComponent } from './template/nav-bar/nav-bar.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    NavBarComponent,
    FooterComponent,
    ModalPerguntasComponent,
    LoadingComponent,
    ControlErrorComponent,
    AbasComponent
  ],
  imports: [NgbModule, CommonModule, RouterModule],
  exports: [
    NavBarComponent,
    FooterComponent,
    LoadingComponent,
    ModalPerguntasComponent,
    ControlErrorComponent,
    AbasComponent
  ],
})
export class SharedModule {}
