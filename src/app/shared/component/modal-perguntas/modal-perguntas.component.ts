import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-perguntas',
  templateUrl: './modal-perguntas.component.html',
  styleUrls: ['./modal-perguntas.component.scss'],
  styles: [
    `
      .dark-modal .modal-content {
        background-color: rgb(210, 210, 211);
        color: white;
        border-radius: 20px;
      }
      .dark-modal .close {
        color: white;
      }
      .light-blue-backdrop {
        background-color: #5cb3fd;
      }
    `,
  ],
})
export class ModalPerguntasComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
