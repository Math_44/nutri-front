import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-abas',
  templateUrl: './abas.component.html',
  styleUrls: ['./abas.component.scss'],
})
export class AbasComponent implements OnInit {
  @Input() typeText: any;
  activate = 0;

  @Output() changeContent: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  showText(type: any) {
    this.changeContent.emit(type);
    this.activate = type;
  }
}
